namespace McDonaldsDomain.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        AddressId = c.Guid(nullable: false),
                        StreetName = c.String(maxLength: 256),
                        StreetNumber = c.String(maxLength: 256),
                        City = c.String(maxLength: 128),
                        Country = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AddressId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Guid(nullable: false),
                        AddressId = c.Guid(nullable: false),
                        CompanyId = c.Guid(nullable: false),
                        FirstName = c.String(maxLength: 128),
                        LastName = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Address", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.Company", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.AddressId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        CompanyId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CompanyId);
            
            CreateTable(
                "dbo.EmployeeLeaveRequest",
                c => new
                    {
                        EmployeeLeaveRequestId = c.Guid(nullable: false),
                        EmployeeId = c.Guid(nullable: false),
                        DateFrom = c.DateTime(),
                        DateTo = c.DateTime(),
                        Comments = c.String(maxLength: 512),
                        RequestedDate = c.DateTime(),
                        Approved = c.Boolean(),
                        ApprovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.EmployeeLeaveRequestId)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeLeaveRequest", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Employees", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.Employees", "AddressId", "dbo.Address");
            DropIndex("dbo.EmployeeLeaveRequest", new[] { "EmployeeId" });
            DropIndex("dbo.Employees", new[] { "CompanyId" });
            DropIndex("dbo.Employees", new[] { "AddressId" });
            DropTable("dbo.EmployeeLeaveRequest");
            DropTable("dbo.Company");
            DropTable("dbo.Employees");
            DropTable("dbo.Address");
        }
    }
}
