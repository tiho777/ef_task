﻿using McDonaldsDomain.Classes.Models;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace McDonaldsDomain.DataModel
{
    public class McDonaldsContext : DbContext
    {
        public McDonaldsContext() { }
        public McDonaldsContext(string connectionString) : base(connectionString) { }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<EmployeeLeaveRequest> EmployeeLeaveRequests { get; set; }
        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //if (this.Database.Connection.Database == "McDonaldsBrda")
            //{
            //    modelBuilder.Entity<Employees>().Ignore(x => x.CompanyId);
            //    modelBuilder.Entity<Employees>().Ignore(x => x.Company);
            //    modelBuilder.Entity<Company>().Ignore(x => x.CompanyEmployees);
            //}

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
