﻿using System;
using System.ComponentModel.DataAnnotations;

namespace McDonaldsDomain.Classes.Models
{
    public class EmployeeLeaveRequest
    {
        [Key]
        public Guid EmployeeLeaveRequestId { get; set; }
        public Employees Employees { get; set; }
        public Guid EmployeeId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        [StringLength(512)]
        public string Comments { get; set; }
        public DateTime? RequestedDate { get; set; }
        public bool? Approved { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
