﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace McDonaldsDomain.Classes.Models
{
    public class Company
    {
        public Company()
        {
            CompanyEmployees = new List<Employees>();
        }

        [Key]
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public List<Employees> CompanyEmployees { get; set; }
    }
}
