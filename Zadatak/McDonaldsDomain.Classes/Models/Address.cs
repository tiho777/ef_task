﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace McDonaldsDomain.Classes.Models
{
    public class Address
    {
        public Address()
        {
            CompanyEmployees = new List<Employees>();
        }

        [Key]
        public Guid AddressId { get; set; }
        public List<Employees> CompanyEmployees { get; set; }
        [StringLength(256)]
        public string StreetName { get; set; }
        [StringLength(256)]
        public string StreetNumber { get; set; }
        [StringLength(128)]
        public string City { get; set; }
        [StringLength(128)]
        public string Country { get; set; }
    }
}
