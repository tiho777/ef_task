﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace McDonaldsDomain.Classes.Models
{
    public class Employees
    {
        public Employees()
        {
            EmployeeLeaveRequests = new List<EmployeeLeaveRequest>();
        }

        [Key]
        public Guid EmployeeId { get; set; }
        public Address Address { get; set; }
        public Company Company { get; set; }
        public List<EmployeeLeaveRequest> EmployeeLeaveRequests { get; set; }
        public Guid AddressId { get; set; }
        public Guid CompanyId { get; set; }
        [StringLength(128)]
        public string FirstName { get; set; }
        [StringLength(128)]
        public string LastName { get; set; }
    }
}
