﻿using McDonaldsDomain.Classes.Models;

using Zadatak.DataTransferObjects;

namespace Zadatak.Extensions
{
    public static class ContextDataExtensions
    {
        public static EmployeeLeaveRequest EmployeeLeaveRequestData(this EmployeeLeaveRequestsDTO target)
        {
            return new EmployeeLeaveRequest()
            {
                EmployeeLeaveRequestId = target.EmployeeLeaveRequestId,
                DateFrom = target.DateFrom,
                DateTo = target.DateTo,
                Comments = target.Comments,
                RequestedDate = target.RequestedDate,
                Approved = target.Approved,
                ApprovedDate = target.ApprovedDate,
                EmployeeId = target.EmployeeId
            };
        }

        public static Address AddressData(this AddressDTO target)
        {
            return new Address()
            {
                AddressId = target.AddressId,
                City = target.City,
                Country = target.Country,
                StreetName = target.StreetName,
                StreetNumber = target.StreetNumber
            };
        }

        public static Company CompanyData(this CompanyDTO target)
        {
            return new Company()
            {
                CompanyId = target.CompanyId,
                Name = target.Name
            };
        }

        public static Employees EmployeeData(this EmployeeDTO target)
        {
            return new Employees()
            {
                EmployeeId = target.EmployeeId,
                AddressId = target.AddressId,
                CompanyId = target.CompanyId,
                FirstName = target.FirstName,
                LastName = target.LastName
            };
        }
    }
}
