﻿using System;

namespace Zadatak.DataTransferObjects
{
    public class EmployeeLeaveRequestsDTO
    {
        public Guid EmployeeLeaveRequestId { get; set; }
        public Guid EmployeeId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string Comments { get; set; }
        public DateTime? RequestedDate { get; set; }
        public bool? Approved { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
