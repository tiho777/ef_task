﻿using System;

namespace Zadatak.DataTransferObjects
{
    public class AddressDTO
    {
        public Guid AddressId { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
