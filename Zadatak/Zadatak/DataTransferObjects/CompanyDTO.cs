﻿using System;

namespace Zadatak.DataTransferObjects
{
    public class CompanyDTO
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
    }
}
