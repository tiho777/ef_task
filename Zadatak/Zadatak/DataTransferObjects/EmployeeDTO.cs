﻿using System;

namespace Zadatak.DataTransferObjects
{
    public class EmployeeDTO
    {
        public Guid EmployeeId { get; set; }
        public Guid AddressId { get; set; }
        public Guid CompanyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
