﻿using McDonaldsDomain.DataModel;

using System.Linq;
using Zadatak.DataTransferObjects;

namespace Zadatak.Helpers
{
    public class ContextDataReaders
    {
        public CompanyDTO GetCompanies(McDonaldsContext context)
        {
            return (from c in context.Companies
                    select new CompanyDTO
                    {
                        CompanyId = c.CompanyId,
                        Name = c.Name
                    }).FirstOrDefault();
        }

        public AddressDTO GetAddresses(McDonaldsContext context)
        {
            return (from a in context.Addresses
                    select new AddressDTO
                    {
                        AddressId = a.AddressId,
                        StreetName = a.StreetName,
                        StreetNumber = a.StreetNumber,
                        City = a.City,
                        Country = a.Country
                    }).FirstOrDefault();
        }

        public EmployeeDTO GetEmployees(McDonaldsContext context)
        {
            return (from p in context.Employees
                    select new EmployeeDTO
                    {
                        EmployeeId = p.EmployeeId,
                        AddressId = p.AddressId,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        CompanyId = context.Companies.Select(g => g.CompanyId).FirstOrDefault()
                    }).FirstOrDefault();
        }

        public EmployeeLeaveRequestsDTO GetEmployeeLeaveRequests(McDonaldsContext context)
        {
            return (from p in context.EmployeeLeaveRequests
                    select new EmployeeLeaveRequestsDTO
                    {
                        EmployeeLeaveRequestId = p.EmployeeLeaveRequestId,
                        DateFrom = p.DateFrom,
                        DateTo = p.DateTo,
                        Comments = p.Comments,
                        RequestedDate = p.RequestedDate,
                        Approved = p.Approved,
                        ApprovedDate = p.ApprovedDate,
                        EmployeeId = context.Employees.Select(g => g.EmployeeId).FirstOrDefault()
                    }).FirstOrDefault();
        }
    }
}
