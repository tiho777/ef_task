﻿using McDonaldsDomain.Classes.Models;
using McDonaldsDomain.DataModel;

using System;
using System.Configuration;
using System.Data.Entity.Validation;

using Zadatak.Extensions;
using Zadatak.Helpers;

namespace Zadatak
{
    public class DataTransferService
    {
        private static readonly string BrdaConnection = ConfigurationManager.ConnectionStrings["Brda"].Name;
        private static readonly string ZnjanConnection = ConfigurationManager.ConnectionStrings["Znjan"].Name;

        public void TransferData()
        {
            Console.WriteLine(string.Format("Birajte izmedju dvije baze podataka:\t{0} / {1}", BrdaConnection, ZnjanConnection));
            var dataBase = Console.ReadLine();

            using (var context = new McDonaldsContext(dataBase))
            {
                var contextReaders = new ContextDataReaders();

                var companyInfoDTO = contextReaders.GetCompanies(context);
                var addressDTO = contextReaders.GetAddresses(context);
                var employeeDTO = contextReaders.GetEmployees(context);
                var employeeLeaveRequestDTO = contextReaders.GetEmployeeLeaveRequests(context);

                Company companyInfo = ContextDataExtensions.CompanyData(companyInfoDTO);
                Address address = ContextDataExtensions.AddressData(addressDTO);
                Employees employee = ContextDataExtensions.EmployeeData(employeeDTO);
                EmployeeLeaveRequest employeeLeaveRequest = ContextDataExtensions.EmployeeLeaveRequestData(employeeLeaveRequestDTO);

                try
                {
                    using (var defaultContext = new McDonaldsContext())
                    {
                        defaultContext.Companies.Add(companyInfo);
                        defaultContext.Addresses.Add(address);
                        defaultContext.Employees.Add(employee);
                        defaultContext.EmployeeLeaveRequests.Add(employeeLeaveRequest);

                        defaultContext.SaveChanges();
                    }
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Console.WriteLine("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);

                        }
                    }

                    Console.ReadLine();
                }

                Console.WriteLine(string.Format("Podaci iz baze podataka {0} uspjesno prebacani", dataBase));
                Console.ReadLine();
            }
        }

    }
}
