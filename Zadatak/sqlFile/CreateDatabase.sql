USE master
CREATE DATABASE [McDonaldsDomain.DataModel.McDonaldsContext]
GO
USE McDonaldsDomain.DataModel.McDonaldsContext
CREATE TABLE [dbo].[Address] (
    [AddressId] [uniqueidentifier] NOT NULL,
    [StreetName] [nvarchar](256),
    [StreetNumber] [nvarchar](256),
    [City] [nvarchar](128),
    [Country] [nvarchar](128),
    CONSTRAINT [PK_dbo.Address] PRIMARY KEY ([AddressId])
)
CREATE TABLE [dbo].[Employees] (
    [EmployeeId] [uniqueidentifier] NOT NULL,
    [AddressId] [uniqueidentifier] NOT NULL,
    [CompanyId] [uniqueidentifier] NOT NULL,
    [FirstName] [nvarchar](128),
    [LastName] [nvarchar](128),
    CONSTRAINT [PK_dbo.Employees] PRIMARY KEY ([EmployeeId])
)
CREATE INDEX [IX_AddressId] ON [dbo].[Employees]([AddressId])
CREATE INDEX [IX_CompanyId] ON [dbo].[Employees]([CompanyId])
CREATE TABLE [dbo].[Company] (
    [CompanyId] [uniqueidentifier] NOT NULL,
    [Name] [nvarchar](max),
    CONSTRAINT [PK_dbo.Company] PRIMARY KEY ([CompanyId])
)
CREATE TABLE [dbo].[EmployeeLeaveRequest] (
    [EmployeeLeaveRequestId] [uniqueidentifier] NOT NULL,
    [EmployeeId] [uniqueidentifier] NOT NULL,
    [DateFrom] [datetime],
    [DateTo] [datetime],
    [Comments] [nvarchar](512),
    [RequestedDate] [datetime],
    [Approved] [bit],
    [ApprovedDate] [datetime],
    CONSTRAINT [PK_dbo.EmployeeLeaveRequest] PRIMARY KEY ([EmployeeLeaveRequestId])
)
CREATE INDEX [IX_EmployeeId] ON [dbo].[EmployeeLeaveRequest]([EmployeeId])
ALTER TABLE [dbo].[Employees] ADD CONSTRAINT [FK_dbo.Employees_dbo.Address_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]) ON DELETE CASCADE
ALTER TABLE [dbo].[Employees] ADD CONSTRAINT [FK_dbo.Employees_dbo.Company_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]) ON DELETE CASCADE
ALTER TABLE [dbo].[EmployeeLeaveRequest] ADD CONSTRAINT [FK_dbo.EmployeeLeaveRequest_dbo.Employees_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Employees] ([EmployeeId]) ON DELETE CASCADE

INSERT INTO [dbo].[Company] ([CompanyId], [Name]) VALUES (N'43e52c14-10c6-4bdf-80a4-73afa51bd628', N'Brda')
INSERT INTO [dbo].[Company] ([CompanyId], [Name]) VALUES (N'371e7e80-d099-4fb0-9334-9da8d1f10ba8', N'znjan')

INSERT INTO [dbo].[Address] ([AddressId], [StreetName], [StreetNumber], [City], [Country]) VALUES (N'5f032ad7-3e1d-4a3b-9ae8-a26bde46a56a', N'znjanska', N'2', N'split', N'croatia')
INSERT INTO [dbo].[Address] ([AddressId], [StreetName], [StreetNumber], [City], [Country]) VALUES (N'14248b20-6f34-4d3d-a08e-de9429d716fb', N'brda', N'2', N'split', N'croatia')

INSERT INTO [dbo].[Employees] ([EmployeeId], [AddressId], [CompanyId], [FirstName], [LastName]) VALUES (N'cad38a9b-ca77-4aa8-8a97-40d4f293c8f4', N'14248b20-6f34-4d3d-a08e-de9429d716fb', N'43e52c14-10c6-4bdf-80a4-73afa51bd628', N'ivo', N'sBrda')
INSERT INTO [dbo].[Employees] ([EmployeeId], [AddressId], [CompanyId], [FirstName], [LastName]) VALUES (N'84036d7b-a2c8-4ddb-9646-a8760b226f1d', N'5f032ad7-3e1d-4a3b-9ae8-a26bde46a56a', N'371e7e80-d099-4fb0-9334-9da8d1f10ba8', N'mate', N'saZnjana')

INSERT INTO [dbo].[EmployeeLeaveRequest] ([EmployeeLeaveRequestId], [EmployeeId], [DateFrom], [DateTo], [Comments], [RequestedDate], [Approved], [ApprovedDate]) VALUES (N'8ea16a3c-d128-4385-a2d2-0a7c6fdbf0cf', N'84036d7b-a2c8-4ddb-9646-a8760b226f1d', N'2018-01-01 00:00:00', N'2018-01-02 00:00:00', N'komentar', N'2017-01-01 00:00:00', 0, NULL)
INSERT INTO [dbo].[EmployeeLeaveRequest] ([EmployeeLeaveRequestId], [EmployeeId], [DateFrom], [DateTo], [Comments], [RequestedDate], [Approved], [ApprovedDate]) VALUES (N'0c73a57a-6247-48a3-8569-d26f14771d8a', N'cad38a9b-ca77-4aa8-8a97-40d4f293c8f4', N'2001-01-01 00:00:00', N'2002-01-01 00:00:00', N'comment', N'2000-01-02 00:00:00', 1, N'2000-01-02 00:00:00')